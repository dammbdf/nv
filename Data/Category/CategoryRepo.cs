﻿using System;
using System.Collections.Generic;
using Core.Category;
using Core.Category.Out;

namespace Data.CategoryB
{
    public class CategoryRepo : ICategoryRepo
    {
        private nvDbContext Context { get; set; }


        public CategoryRepo(nvDbContext context)
        {
            Context = context;
        }

        
        public IEnumerable<CategoryObject> ReadCategories() // TODO
        {
            var data = Context.Categories.AsQueryable();
            
            var res = data.ToGategoryObjects();
            
            return res;
        }
    }
}