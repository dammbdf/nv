﻿using System.Collections.Generic;
using System.Linq;
using Core.Category;
using Data.Entities;

namespace Data.CategoryB
{
    public static class CategoryHelper
    {
        public static CategoryObject ToCategoryObject(this Category category)
        {
            return new CategoryObject()
            {
                Id = category.Id,

                Name = category.Name,
                Description = category.Description,

                AddDate = category.AddDate,
                LastEditDate = category.LastEditDate
            };
        }

        public static IEnumerable<CategoryObject> ToGategoryObjects(this IQueryable<Category> categories)
        {
            return categories.Select(c => new CategoryObject()
            {
                Id = c.Id,

                Name = c.Name,
                Description = c.Description,

                AddDate = c.AddDate,
                LastEditDate = c.LastEditDate
            });
        }
    }
}