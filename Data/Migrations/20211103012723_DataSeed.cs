﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class DataSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "AddDate", "Description", "LastEditDate", "Name" },
                values: new object[,]
                {
                    { -1, new DateTime(2021, 11, 3, 3, 27, 22, 743, DateTimeKind.Local).AddTicks(9796), "Good stuff", null, "Fruits" },
                    { -2, new DateTime(2021, 11, 3, 3, 27, 22, 747, DateTimeKind.Local).AddTicks(1086), "Even better stuff", null, "Vegetables" },
                    { -3, new DateTime(2021, 11, 3, 3, 27, 22, 747, DateTimeKind.Local).AddTicks(1124), "Stuff from things that moos or bleats", null, "Dairy" },
                    { -4, new DateTime(2021, 11, 3, 3, 27, 22, 747, DateTimeKind.Local).AddTicks(1128), "All things that swims, walk or fly", null, "Meats" },
                    { -5, new DateTime(2021, 11, 3, 3, 27, 22, 747, DateTimeKind.Local).AddTicks(1131), null, null, "Others" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "AddDate", "CategoryId", "Code", "IsActive", "LastEditDate", "ManufactureDate", "Name" },
                values: new object[,]
                {
                    { -11, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(8377), -1, "APPL10", true, null, new DateTime(2021, 11, 2, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9152), "Apple" },
                    { -12, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9949), -1, "ORA", true, null, new DateTime(2021, 11, 2, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9961), "Orange" },
                    { -13, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9965), -1, "3500Ben", true, null, new DateTime(2021, 11, 1, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9967), "Banana" },
                    { -21, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9969), -2, "LET", true, null, new DateTime(2021, 11, 1, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9972), "Letuce" },
                    { -22, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9974), -2, "CRI42", true, null, new DateTime(2021, 10, 31, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9976), "Onions" },
                    { -23, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9978), -2, "LET", true, null, new DateTime(2021, 10, 27, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9981), "Letuce" },
                    { -31, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9983), -3, "cow56", true, null, new DateTime(2021, 11, 1, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9985), "Milk" },
                    { -32, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9987), -3, "14", true, null, new DateTime(2021, 10, 27, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9990), "Cheese" },
                    { -33, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9992), -3, "YO453", true, null, new DateTime(2021, 10, 31, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9994), "Yogurt" },
                    { -41, new DateTime(2021, 11, 3, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9996), -4, "coq", true, null, new DateTime(2021, 10, 27, 3, 27, 22, 748, DateTimeKind.Local).AddTicks(9998), "Poultry" },
                    { -42, new DateTime(2021, 11, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(1), -4, "OINK", true, null, new DateTime(2021, 10, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(3), "Pork" },
                    { -43, new DateTime(2021, 11, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(14), -4, "433", true, null, new DateTime(2021, 10, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(17), "Tuna" },
                    { -51, new DateTime(2021, 11, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(19), -5, "TP", true, null, new DateTime(2021, 11, 2, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(21), "Toilet paper" },
                    { -52, new DateTime(2021, 11, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(24), -5, "binks", true, null, new DateTime(2020, 11, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(26), "Jar" },
                    { -53, new DateTime(2021, 11, 3, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(42), -5, "rsss4000", true, null, new DateTime(2021, 10, 31, 3, 27, 22, 749, DateTimeKind.Local).AddTicks(44), "Matches" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -53);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -52);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -51);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -43);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -42);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -41);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -33);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -32);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -31);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -23);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -22);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -21);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -13);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -12);

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: -11);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: -5);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: -4);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: -3);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: -2);

            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: -1);
        }
    }
}
