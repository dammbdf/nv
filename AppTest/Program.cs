﻿using Data;
using Data.Entities;
using System;

namespace AppTest
{
	internal class Program
	{
		static void Main(string[] args)
		{
			//AddCategories();
			
			PrintCategories();
		}


		private static void AddCategories()
		{
			using (var db = new nvDbContext())
			{
				db.Categories.AddRange(new[] {
					new Category { Name = "asd"},
					new Category { Name ="dsa"} });

				db.SaveChanges();
			}
		}

		private static void PrintCategories()
		{
			using (var db = new nvDbContext())
			{
				var cats = db.Categories.AsQueryable();

				foreach (var cat in cats)
				{
					Console.WriteLine(cat.Name);
				}
			}
		}

	}
}
