﻿using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Linq;
using System.Text.Encodings.Web;

namespace App.Controllers
{
	public class ProductsController : Controller
	{
		public IActionResult Index()
		{
			ViewData["products"] = (new string[] { "asd", "dsa" }).FirstOrDefault();
			ViewData["count"] = 3;

			return View();
		}

		public IActionResult Welcome(string name, int numTimes = 1)
		{
			ViewData["Message"] = $"Hello {name}";
			ViewData["NumTimes"] = numTimes;

			return View();
		}
	}
}
