﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Category.In
{
    public static class CategoryVMhelper
    {
        public static IEnumerable<CategoryVM> ToGategoryVM(this IEnumerable<CategoryObject> categories)
        {
            return categories.Select(c => new CategoryVM()
            {
                Id = c.Id,

                Name = c.Name,
                Description = c.Description,

                AddDate = c.AddDate,
                LastEditDate = c.LastEditDate
            });
        }
    }
}