﻿using System.Collections.Generic;

namespace Core.Category.In
{
    public interface ICategoryService
    {
        public IEnumerable<CategoryVM> GetCategories();
    }
}