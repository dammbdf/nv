﻿using System;
using System.Collections.Generic;

namespace Core.Category.In
{
    public class CategoryService : ICategoryService
    {
        private CategoryCore Core { get; set; }


        public CategoryService(CategoryCore core)
        {
            Core = core;
        }

        
        public IEnumerable<CategoryVM> GetCategories()
        {
            var cats = Core.GetCategories();
            
            var res = cats.ToGategoryVM();

            return res;
        }
    }
}