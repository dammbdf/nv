﻿using System.Collections.Generic;

namespace Core.Category.Out
{
    public interface ICategoryRepo
    {
        public IEnumerable<CategoryObject> ReadCategories();
    }
}