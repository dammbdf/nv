﻿using System.Collections;
using System.Collections.Generic;
using Core.Category.Out;

namespace Core.Category
{
    public class CategoryCore
    {
        private ICategoryRepo CategoryRepo { get; set; }


        public CategoryCore(ICategoryRepo categoryRepo)
        {
            CategoryRepo = categoryRepo;    
        }

        
        public IEnumerable<CategoryObject> GetCategories()
        {
            return CategoryRepo.ReadCategories();
        }
    }
}