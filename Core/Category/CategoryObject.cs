﻿using System;
using System.Collections.Generic;

namespace Core.Category
{
    public class CategoryObject
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime AddDate { get; set; } = DateTime.Now;
        public DateTime? LastEditDate { get; set; }

        // TODO
        //public virtual ICollection<Product> Products { get; set; }
    }
}