
Write a simple web application using C# language, to manage products and product categories.

## Reqs

1. Categories definition should contain at least _name_, _description_, _added date_, _last edited date_.
	- There should not be two or more categories with the same _name_.
2. Product definition should contain at least
	- _product code_, _name_, _category_, _unit price_, _manufacture date_, _active flag_, _added date_, _last edited date_
	(highlighted info would be mandatory).
	- There should not be two or more products with the same code.
1. Page to display all the active products
	- Columns: Running number, Prod Code, Prod Name, Category (Name), Unit price, Date of manufacture
	- Add filter for Category
2. Page for Add/edit product

## NFRs
#### Key goals:

- Database definition:
	- data types (nullable / mandatory)
	- foreign key
	- unique constraints
	- data seed (2-3 product categories with 2-3 products in each)
- User input validation
	(data type check, required fields, foreign keys, unique keys)
- Clean coding

#### Not in this scope:

- Authentication
- Authorization
- Application visual style

## Tech Stack

1. Interaction - Asp.Net
2. Persistance - MS SQL Server
