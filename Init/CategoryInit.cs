﻿using Core.Category;
using Core.Category.In;
using Core.Category.Out;
using Data;
using Data.CategoryB;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Init
{
    public static class CategoryInit
    {
        public static void AddCategory(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<CategoryCore>();
            services.AddTransient<ICategoryRepo, CategoryRepo>();
            services.AddTransient<nvDbContext>();
        }
    }
}