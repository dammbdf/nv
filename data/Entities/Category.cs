﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
    public partial class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime AddDate { get; set; } = DateTime.Now;
        public DateTime? LastEditDate { get; set; }

        public virtual ICollection<Product> Products { get; set; }

        public Category()
        {
            Products = new List<Product>();
        }
    }
}
