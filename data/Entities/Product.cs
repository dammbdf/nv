﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Data.Entities
{
	public partial class Product
	{
		public int Id { get; set; }

		public string Name { get; set; }
		public string Code { get; set; }
		public DateTime? ManufactureDate { get; set; }

		public int CategoryId { get; set; }
		public Category Category { get; set; }

		public DateTime AddDate { get; set; } = DateTime.Now;
		public DateTime? LastEditDate { get; set; }

		public bool IsActive { get; set; } = true;

	}
}
