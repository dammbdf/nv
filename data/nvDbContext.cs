﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Data
{
	public class nvDbContext : DbContext
	{
		public DbSet<Category> Categories { get; set; }
		public DbSet<Product> Products { get; set; }

		public string ConnectionString { get; private set; }

		//"server=.;Database=nv1;Trusted_Connection=True;"
		//"Server=.;Database=nv1;UID=sa;PWD=cormoran21;"
		public nvDbContext(string connectionString = "Server=.;Database=nv1;UID=sa;PWD=cormoran21;")
		{
			ConnectionString = connectionString;
		}


		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
			=> optionsBuilder.UseSqlServer(ConnectionString);

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			/*
			modelBuilder.Entity<Category>(c =>
			{
				c.Property(p => p.Name).IsRequired();
				c.HasIndex(i => i.Name, "Category.Name.IsUnique").IsUnique();

				c.Property(p => p.AddDate).HasDefaultValueSql("GETDATE()");
			});

			modelBuilder.Entity<Product>(p =>
			{
				p.Property(p => p.Name).IsRequired();

				p.Property(p => p.Code).IsRequired();
				p.HasIndex(i => i.Code, "Product.Code.IsUnique").IsUnique();

				p.Property(p => p.CategoryId).IsRequired();
				p.HasIndex(i => i.CategoryId).IsClustered(false);

				p.Property(p => p.AddDate).HasDefaultValueSql("GETDATE()");

				p.Property(p => p.IsActive).HasDefaultValue(true);
			});
			*/

			modelBuilder.Entity<Category>().HasData(SeedCategories);
			modelBuilder.Entity<Product>().HasData(SeedProducts);
		}

		private Category[] SeedCategories => new Category[] {
			new() { Id=-1, Name = "Fruits", Description = "Good stuff" },
			new() { Id=-2, Name = "Vegetables", Description = "Even better stuff" },
			new() { Id=-3, Name = "Dairy", Description = "Stuff from things that moos or bleats" },
			new() { Id=-4, Name = "Meats", Description = "All things that swims, walk or fly" },
			new() { Id=-5, Name = "Others"},
		};

		private Product[] SeedProducts => new Product[] {
			new() { Id=-11, Name ="Apple", Code = "APPL10", ManufactureDate = DateTime.Now.AddDays(-1), CategoryId = -1},
			new() { Id=-12, Name ="Orange", Code = "ORA", ManufactureDate = DateTime.Now.AddDays(-1), CategoryId = -1},
			new() { Id=-13, Name ="Banana", Code = "3500Ben", ManufactureDate = DateTime.Now.AddDays(-2), CategoryId = -1},

			new() { Id=-21, Name ="Lettuce", Code = "LET", ManufactureDate = DateTime.Now.AddDays(-2), CategoryId = -2},
			new() { Id=-22, Name ="Onions", Code = "CRI42", ManufactureDate = DateTime.Now.AddDays(-3), CategoryId = -2},
			new() { Id=-23, Name ="Potatoes", Code = "LET", ManufactureDate = DateTime.Now.AddDays(-7), CategoryId = -2},

			new() { Id=-31, Name ="Milk", Code = "cow56", ManufactureDate = DateTime.Now.AddDays(-2), CategoryId = -3},
			new() { Id=-32, Name ="Cheese", Code = "14", ManufactureDate = DateTime.Now.AddDays(-7), CategoryId = -3},
			new() { Id=-33, Name ="Yogurt", Code = "YO453", ManufactureDate = DateTime.Now.AddDays(-3), CategoryId = -3},

			new() { Id=-41, Name ="Poultry", Code = "coq", ManufactureDate = DateTime.Now.AddDays(-7), CategoryId = -4},
			new() { Id=-42, Name ="Pork", Code = "OINK", ManufactureDate = DateTime.Now.AddMonths(-1), CategoryId = -4},
			new() { Id=-43, Name ="Tuna", Code = "433", ManufactureDate = DateTime.Now.AddMonths(-1), CategoryId = -4},

			new() { Id=-51, Name ="Toilet paper", Code = "TP", ManufactureDate = DateTime.Now.AddDays(-1), CategoryId = -5},
			new() { Id=-52, Name ="Jar", Code = "binks", ManufactureDate = DateTime.Now.AddYears(-1), CategoryId = -5},
			new() { Id=-53, Name ="Matches", Code = "rsss4000", ManufactureDate = DateTime.Now.AddDays(-3), CategoryId = -5},
		};
	}
}
